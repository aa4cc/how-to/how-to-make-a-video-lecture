# How to make a video lecture

The goal: a video-lecture typeset in Latex, lecturer himeself not visible, just the presented material and the lecturer's voice, gradual uncovering of individual components on the slide, black background, video finally placed on Youtube.

The workflow is
1. Prepare the presentation in Latex (possibly in MS Powerpoint or LibreOffice Present)
2. Based on the presentation, write down the screenplay (exact wording)
3. Record your reading out loud the screenplay together with recording the screencast of the presentation.
4. Upload on Youtube

## Preparing the presentation
Preferably using Latex and [Beamer](https://ctan.org/pkg/beamer?lang=en) package. [Direct link to manual [PDF]](https://www.google.com/search?client=ubuntu&channel=fs&q=pause+beamer&ie=utf-8&oe=utf-8).

In the preambule of the Latex source file put

```latex
\documentclass{beamer}
```

### Basic usage of Beamer
A minimalistic latex source is

```latex
\documentclass{beamer}

\usepackage[utf8x]{inputenc}
% other packages here


\title{Nadpis}
\subtitle{Podnadpis}
\author{Jan Novák}
\institute{Department of Control Engineering\\Faculty of Electrical Engineering\\Czech Technical University in Prague}
\date{\today}

\begin{document}

\frame{\titlepage}

\frame{\frametitle{Outline}\tableofcontents}

\section{První sekce, bude vidět jen v obsahu}

\begin{frame}
\frametitle{Nadpis prvního slajdu}
Samotný text prvního slajdu
\end{frame}

\begin{frame}
\frametitle{Nadpis druhého slajdu}
Samotný obsah druhého slajdu
\end{frame}

\end{document}

```

### Partial uncovering of individual compoments on the slide

Just using `\pause` command at the point where pause is expexted seems nearly enough. Section 9.1, page 80 in the [PDF manual](https://www.google.com/search?client=ubuntu&channel=fs&q=pause+beamer&ie=utf-8&oe=utf-8).

### Dark theme

#### In Latex/Beamer
A dark theme in Beamer can be enforced through `\usetheme` and `\usecolortheme`. Without much exploration, the following seems to do a decent job

```latex
\documentclass{beamer}
\usetheme{Pittsburgh}
\usecolortheme{owl}
```

### Handouts
If ultimately some handouts are also needed, it can be done by definining a new *mode* somewhere before the `\begin{document}`

```latex
\mode<handout>{%
  \pgfpagesuselayout{4 on 1}[a4paper,landscape,border shrink=5mm]
}
```
and then the very first line of the file is changed to

```latex
\documentclass[handout]{beamer}
```

### Graphics
With black bacground, the graphics created for a paper will typically need to be reverted. This certainly holds for bitmap graphics (png, jpg) but also for vector graphics (pdf). The trouble is that while using the dark *theme* in Latex/Beamer, some vector compoments of the vector grapics are automatically made compatible with the dark mode but not all (typically axis labels with graphs). Therefore, it is safer to convert the images by yourself.  

#### In Matlab
Using the third-party [Quick Dark or Custom Plot Background](https://www.mathworks.com/matlabcentral/fileexchange/30222-quick-dark-or-custom-plot-background) file.

```matlab
h = figure(1)
plot(1:10,cos(1:10))
xlabel('x1')
ylabel('x2')
darkBackground(h)
```

For some reason, the following line should be run before exporting the Matlab figure to some other format

```matlab
set(gcf, 'InvertHardcopy', 'off')
```

Exporting to PDF is best done by exporting first to eps format and then using **epstopdf**. Directly exporting to PDF creates unnecessarily large margins.

```matlab
print -depsc2 ../figures/myplot.eps
!epstopdf ../figures/myplot.eps

```


#### In Julia
Using [PlotThemes](https://github.com/JuliaPlots/PlotThemes.jl) package.

```julia
add PlotThemes
```

```julia
using Plots
theme(:dark)
```
## Preparing the text for the audio

Write down the exact wording of the speech using any text editor. This is another occassion in which the lecture can be really finetuned. Be short, be precise.

It is helpful to indicate in (almost) every sentence, where the **emphasis** is to be placed while reading - use bold face for this.

Structure the text visibly into paragraphs, the end of the paragraph signalling that another item in the  presentation is to be uncovered by pressing a relevant key in the presentation sofware (PDF viewer in the case of a presentation rendered in PDF).

It is useful to indicate into the text the end of the slide, for example by using a horizontal line. This then serves as an undisturbing synchronization mark.

## Recording video and audio
Gazillions of softwares for recording screens (screncasting). For simple needs, simple software suffices.

One such software is [Vokoscreen](https://linuxecke.volkoh.de/vokoscreen/vokoscreen.html) - available all Windows and Linux.

For the whole procedure, if two monitors are available, it is ideal, but one is also fine. It is not just a matter of space but rather that in the two-monitor situation, the presentation can go in the full-screen mode on one monitor while some other stuff can be still accessible from the other monitor.

The procedure:
1. In the two-monitor scenario, move the pdf viewer with the presentation PDF file on one of the two monitors and switch to the full screen mode. In the single-monitor case, you will have to do it just on a part of the monitor but then it will not be possible to switch the presentation to the full screen mode obviously if you still want to have an access to controls for the recording software.
2. Launch the vokoscreen screencasting software. In the two-monitor scenario you will make two selections: 1) pick recording a whole screen and 2) select the right screen/monitor (in the single-monitor setup only a rectangular area must be recorded, not a full window, not a full screen). Select recording of the audio (it will be inserted into the video directly) and choose the proper audio input. Select also the directory into which the recording should be stored.
3. Before recording, three things should be visible: 1) the presentation slides, ideally in the full screen mode, 2) control pannel for screen recording software (vocoscreen), 3) the screenplay text. The latter could be either displayed on the monitor, printed on a sheet of paper, or perhaps even displayed on yet another devices such as tablet or smartphone.  
4. Start recording. Primarily watching the screenplay text and reading it loud, pressing the corresponding key after every end of a paragraph. Keeping half an eye on the presentation monitor, just to check synchronization, for example at the end of the slide (indicated in the text).
5. If you make a mistate while reading out loud, or if you get lost, do not stop recording, just relax, take a deep breath, no problem even 30s or so, return to the beginning of the current slide, located the corresponding text in the screenplay and start doing the current slide again. You will then cut out the spoiled part and glue the rest later.


### Postprocessing of audio and video

Ideally, no postprocessing is needed because the audio track is directly combined with the video track while recording both.

Just in case of some mistakes, video editting sofware might be needed:

- [Kdenlive](https://kdenlive.org/en/) - Linux, Windows, Mac OS. Import the combined audio video, split into separate audio and video, locate the critical segments, cut them out, glue the rest and render a new video. Be ready for at least a few minutes of machine work.
